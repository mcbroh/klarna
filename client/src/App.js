import React, { Fragment } from "react";
import { Route, BrowserRouter as Router } from 'react-router-dom';

import { Header, Footer, OfferList, Authenticate } from './components';
import Invoice from './Invoice';

import './app.scss';


function App() {
    return (
        <Fragment>
            <Header />
            <Router>
                <main role="main">
                    <Route exact path="/" component={OfferList} />
                    <Route path="/invoice" component={Invoice} />
                </main>
            </Router>
            <Footer />
            <Authenticate />
        </Fragment>
    )
}

export default App;


