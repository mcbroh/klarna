import React, { useState, useEffect, Fragment } from 'react';
import axios from 'axios';

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { FlashHeaderAdvert } from './FlashHeaderAdvert';

const telephoneNumber = 'tel:+234777777777';

export function OfferList({ offers }) {

    const [loading, setLoading] = useState(true);
    const [offerSingle, setOfferSingle] = useState([]);
    const [firstThreeOffers, setFirstThreeOffers] = useState([]);
    const [showModal, toggleModal] = useState(false);
    const [selectedOffer, setSelectedOffer] = useState({});

    useEffect(() => {
        axios.get("/offers/list").then(({ data }) => {
            const offerCopy = [...data.offers];
            if (data.offers.length > 3) {
                setFirstThreeOffers(offerCopy.splice(0, 3));
                setOfferSingle(offerCopy);
            } else {
                setOfferSingle(offerCopy);
            };
            setLoading(false);
        });
    }, []);

    function showOfferCondition(offer) {
        setSelectedOffer(offer);
        toggleModal(true);
    }

    function card(offer, showDetails = false, badgeColor = 'grey') {
        return (
            <div className="card mb-4 shadow-sm">
                <img className="bd-placeholder-img card-img-top" src={offer.images[0]} alt={offer.productDescription} width="100%" />
                <div className="card-body">
                    <p className="card-text">{offer.productDescription}</p>
                    {showDetails && <p className="card-text">{offer.details}</p>}
                    <div className="d-flex justify-content-between align-items-center">
                        <div className="btn-group">
                            <button type="button"
                                className="btn btn-sm btn-outline-secondary"
                                onClick={() => showOfferCondition(offer)}>Conditions</button>
                            <a type="button"
                                href={telephoneNumber}
                                className="btn btn-sm btn-outline-primary">Book</a>
                        </div>
                        <span className={`badge badge-pill badge-${getClass(getPiecesLeft(offer))}`}>
                            {getPiecesLeft(offer)} Left
                        </span>
                    </div>
                    <div className="app-badge-overlay">
                        <span className={`top-right app-badge ${badgeColor}`}>Save {getAmount(Number(offer.marketPrice) - Number(offer.offerPrice))}</span>
                    </div>
                </div>
            </div>
        )
    }

    function getClass(left) {
        if (left < 5) return 'danger';
        if (left < 10) return 'warning';
        return 'success'
    }

    function getPiecesLeft(offer) {
        return Number(offer.qualifyingPieces) - offer.booked.length;
    }

    function getAmount(amount) {
        return 'NGN ' + amount + ',000'
    }

    function twoOneList(offers) {
        return (
            <div className="container-fluid">
                <div className="row d-flex">
                    <div className="col-md-4 p-2">
                        <div className="col-12 p-0 h-50">
                            {card(offers[0])}
                        </div>
                        <div className="col-12 p-0 h-50">
                            {card(offers[1])}
                        </div>
                    </div>
                    <div className="col-md-8 p-2">
                        {card(offers[2], true, 'red')}
                    </div>
                </div>
            </div>
        )
    }

    return (
        <Fragment>
            <FlashHeaderAdvert />
            {loading ? (
                <div className="loader">
                    <div className="spinner-border"></div>
                </div>
            ) : (
                <div className="offers-list py-5 bg-light">
                    <div className="container">
                        <div className="row">

                            {firstThreeOffers.length > 0 && twoOneList(firstThreeOffers)}

                            {offerSingle.map((offer, index) => (
                                <div key={index} className="col-md-4 p-2">
                                    {card(offer)}
                                </div>
                            ))}

                        </div>
                        <Modal show={showModal}>
                            <Modal.Header>
                                <h5 class="modal-title">{selectedOffer.productDescription}</h5>
                            </Modal.Header>
                            <Modal.Body>
                                <p>{selectedOffer.conditions}</p>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button
                                    variant='secondary'
                                    onClick={() => toggleModal(!showModal)}>Close</Button>
                                <Button variant='primary' href={telephoneNumber}>Book</Button>
                            </Modal.Footer>
                        </Modal>
                    </div>
                </div>
            )}
        </Fragment>
    )
}