import React from 'react';

export function Footer() {
    return (
        <footer className="text-muted">
            <div className="container">
                <p>&copy; LDB2B</p>
            </div>
        </footer>
    )
}