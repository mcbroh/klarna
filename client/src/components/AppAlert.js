import React from 'react';
import { Alert } from 'react-bootstrap';

import { useUiState, useUiDispatch } from '../hooks';

export function AppAlert() {

    const dispatch = useUiDispatch();
    const { appAlert} = useUiState();

    if (appAlert.message) {
        return (
            <Alert variant={appAlert.variant || 'success'} onClose={() => dispatch({ type: 'alert' })} dismissible>
                {appAlert.message}
            </Alert>
        )
    }
    return null;
}