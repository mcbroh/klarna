import React from 'react';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';

import { useUiDispatch, useUiState } from '../hooks';

export function Header() {

    const dispatch = useUiDispatch();
    const { user } = useUiState();

    return (
        <header>
            <Navbar collapseOnSelect expand="sm" bg="dark" variant="dark">
                <Navbar.Brand href="/">LDB2B</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="ml-auto">
                        <NavDropdown title="Business" id="collasible-nav-dropdown">
                            {!user.token && (
                                <NavDropdown.Item onClick={() => dispatch({ type: 'toggleAuthDialog' })}>
                                    Login
                                </NavDropdown.Item>
                            )}
                            <NavDropdown.Item href="/">How it works</NavDropdown.Item>
                            <NavDropdown.Item href="/">Create account</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item href="/invoice">Trade invoice</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </header>
    )
}