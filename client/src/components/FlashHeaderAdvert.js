import React from 'react';

export function FlashHeaderAdvert({header}) {
    return (
        <section className="jumbotron text-center">
            <div className="container">
                <h1>{header || 'This will hold flash adverts with Images'}</h1>
                <p className="lead text-muted">Something short and leading about the collection below—its contents, the creator, etc. Make it short and sweet, but not too short so folks don’t simply skip over it entirely.</p>
            </div>
        </section>
    )
}