import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';

import {AppAlert} from './AppAlert';
import { useUiState, useUiDispatch } from '../hooks';

const loginDetails = [
    {
        label: 'Email',
        type: 'email',
        key: 'email',
    },
    {
        label: 'Password',
        type: 'password',
        key: 'password',
    }
]

const registerDetails = [
    ...loginDetails,
    {
        label: 'Contact Name',
        type: 'text',
        key: 'contactName',
    },
    {
        label: 'Contact Telephone',
        type: 'tel',
        key: 'contactTelephone',
    },
    {
        label: 'Company Name',
        type: 'text',
        key: 'companyName',
    },
    {
        label: 'Company Email',
        type: 'email',
        key: 'companyEmail',
    },
    {
        label: 'Company Telephone',
        type: 'tel',
        key: 'companyTelephone',
    },
]

export function Authenticate() {

    const [loginForm, setLoginForm] = useState({});
    const [registerForm, setRegisterForm] = useState({});
    const [showSignInForm, toggleForms] = useState(true);
    const [loading, setLoading] = useState(false);
    const dispatch = useUiDispatch();
    const { showAuthDialog } = useUiState();

    function doSubmit() {
        const route = showSignInForm ? 'users/login' : 'users/new';
        const params = showSignInForm ? loginForm : registerForm;

        setLoading(true);

        axios.post(route, params).then(({ data }) => {
            if (data.success) {
                setLoading(false);
                if (!showSignInForm) {
                    showSignInForm(true);
                    dispatch({ type: 'alert', value: { message: 'You are registered, please sign in!' } });
                } else {
                    dispatch({ type: 'updateUser', value: { ...data.user, token: data.token } });
                    dispatch({ type: 'toggleAuthDialog' });
                }
            } else {
                setLoading(false);
                dispatch({ type: 'alert', value: { message: data.message, variant: 'danger' } })
            };
            setLoading(false);
        });
    }

    function displaySignInForm() {
        return (
            <form>
                {loginDetails.map(obj => (
                    <div key={obj.key} className="form-group">
                        <label htmlFor={obj.key}>{obj.label}</label>
                        <input
                            className="form-control"
                            type={obj.type}
                            value={loginForm[obj.key]} id={obj.key}
                            onChange={(e) => setLoginForm({
                                ...loginForm,
                                [obj.key]: e.target.value
                            })} />
                    </div>
                ))}
                <span className="cta" onClick={() => toggleForms(!showSignInForm)}>Register</span>
            </form>
        )
    }

    function displayRegisterForm() {
        return (
            <form>
                {registerDetails.map(obj => (
                    <div key={'reg' + obj.key} className="form-group">
                        <label htmlFor={obj.key}>{obj.label}</label>
                        <input
                            className="form-control"
                            type={obj.type}
                            value={registerForm[obj.key]} id={obj.key}
                            onChange={(e) => setRegisterForm({
                                ...registerForm,
                                [obj.key]: e.target.value
                            })} />
                    </div>
                ))}
                <span className="cta" onClick={() => toggleForms(!showSignInForm)}>Login</span>
            </form>
        )
    }

    const getHeader = () => showSignInForm ? 'Log In' : 'Register';

    return (
        <Modal show={showAuthDialog}>
            <Modal.Header>
                <h5 className="modal-title">{getHeader()}</h5>
            </Modal.Header>
            <Modal.Body>
                <AppAlert />
                <div className="authenticate">
                    {showSignInForm ? displaySignInForm() : displayRegisterForm()}
                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button
                    variant='secondary'
                    onClick={() => dispatch({ type: 'toggleAuthDialog' })}>Close</Button>
                <Button variant='primary' disabled={loading} onClick={() => doSubmit()}>
                    {loading ? 'Please wait...' : 'Submit'}
                </Button>
            </Modal.Footer>
        </Modal>
    )
}