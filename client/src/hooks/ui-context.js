import React, { useReducer, createContext, useContext } from 'react';

const InitialState = {
    showAuthDialog: false,
    user: {},
    appAlert: {
        variant: 'success',
        message: null,
    }
};

const AppStateContext = createContext();
const AppDispatchContext = createContext();

function UiReducer(state, action) {
    switch (action.type) {
        case 'toggleAuthDialog': {
            return { ...state, showAuthDialog: !state.showAuthDialog }
        }
        case 'updateLoginStatus': {
            return { ...state, user: getLocalStorage() }
        }
        case 'updateUser': {
            if (!action.value) {
                new Error(`Unhandled action type, action.value is missing`)
            }
            if (action.value.id) {
                setLocalStorage(action.value);
            }
            return { ...state, user: action.value }
        }
        case 'alert': {
            if (!action.value) {
                return { ...state, appAlert: InitialState.appAlert }
            }
            return { ...state, appAlert: action.value }
        }
        default: {
            throw new Error(`Unhandled action type: ${action.type}`)
        }
    }
}

function UiProvider({ children }) {
    const [state, dispatch] = useReducer(UiReducer, InitialState);

    return (
        <AppStateContext.Provider value={state}>
            <AppDispatchContext.Provider value={dispatch}>
                {children}
            </AppDispatchContext.Provider>
        </AppStateContext.Provider>
    )
}

function setLocalStorage(params) {
    localStorage.setItem('loggedInUser', JSON.stringify(params));
}

function getLocalStorage(params) {
    const userObj = localStorage.getItem('loggedInUser');
    if (userObj) {
        return JSON.parse(userObj)
    }
    return {};
}

function useUiState() {
    const context = useContext(AppStateContext)
    if (context === undefined) {
        throw new Error('useUiState must be used within a UiProvider')
    }
    return context
}

function useUiDispatch() {
    const context = useContext(AppDispatchContext)
    if (context === undefined) {
        throw new Error('useUiDispatch must be used within a UiProvider')
    }
    return context
}

export { UiProvider, useUiState, useUiDispatch }