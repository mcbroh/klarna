import React from 'react';

import {InvoiceList} from './InvoiceList';
import { FlashHeaderAdvert } from '../components/FlashHeaderAdvert';

function Invoice() {

    return (
        <div className="invoice pb-5 bg-light">
            <FlashHeaderAdvert header='Invest in invoices! Make from 5% to 20% return'/>
            <InvoiceList />
        </div>
    )
}

export default Invoice;