import React, { useEffect, useState, Fragment } from 'react';
import axios from 'axios';
import { Button, Alert } from 'react-bootstrap';

import { useUiState, useUiDispatch } from '../hooks';

export function InvoiceList() {
    const [invoices, setInvoices] = useState([]);
    const [loading, setLoading] = useState(true);
    const [canTrade, setCanTrade] = useState(false);

    const { user } = useUiState();
    const dispatch = useUiDispatch();

    useEffect(() => {
        dispatch({type: 'updateLoginStatus'});
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[])

    useEffect(() => {
        axios.get("/invoice/list", {
            headers: {
                'Authorization': 'Bearer ' + user.token
            },
        }).then(({ data }) => {
            setInvoices(data.invoices);
            setCanTrade(data.canTrade);
            setLoading(false);
        });
    }, [user])

    function showMessage(canTrade) {
        return canTrade ? (
            null
        ) : (
            <Alert variant='warning'>
                <span
                    className="cta" 
                    onClick={() => dispatch({ type: 'toggleAuthDialog' })}
                >Login or Register</span> to start trading invoices!
            </Alert>
        )
    }

    return (
        <div className='invoice-list py-5 container'>
            {loading ? (
                <div className="loader">
                    <div className="spinner-border"></div>
                </div>
            ) : (
                <Fragment>
                    {showMessage(canTrade)}
                    <table role="table">
                        <thead>
                            <tr role="row">
                                <th role="columnheader">Seller</th>
                                <th role="columnheader">Buyer</th>
                                <th role="columnheader">Invoice Price</th>
                                <th role="columnheader">Total Price</th>
                                <th role="columnheader">Repayment</th>
                                <th role="columnheader"></th>
                            </tr>
                        </thead>
                        <tbody>
                            {invoices.map((invoice, index) => (
                                <tr key={index} role="row">
                                    <td role="cell">{invoice.owner}</td>
                                    <td role="cell">{invoice.booked[0].name}</td>
                                    <td role="cell">{invoice.invoicePrice}</td>
                                    <td role="cell">{invoice.totalPrice}</td>
                                    <td role="cell">12 months</td>
                                    <td role="cell">
                                        <Button disabled={!canTrade} variant="primary" size='md'>Buy</Button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </Fragment>
            )}
        </div>
    )
}