import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

import { UiProvider } from './hooks';
import App from './App';

import * as serviceWorker from './serviceWorker';

if (process.env.NODE_ENV === 'development') {
    axios.defaults.baseURL = 'http://localhost:8080/';
}

const main = (
    <UiProvider>
        <App />
    </UiProvider>
)
ReactDOM.render(main, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
