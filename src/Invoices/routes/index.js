const express    = require("express");
const router = new express.Router();

const controller = require("../controllers");
const jwt = require('../../services/jwt');

router.post("/new", jwt.authenticateToken, controller.newInvoice);
router.get("/list", jwt.canTrade, controller.list);


module.exports = router;