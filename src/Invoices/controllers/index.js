const Invoices = require('../models');
const validator = require('./validationSchema');

async function newInvoice(req, res) {
    const params = req.body;
    try {
        await validator.createInvoiceValidation.validateAsync(params);
    } catch (error) {
        return res.send({ success: false, error: error.details[0].message });
    }

    try {
        const user = req.user;
        const invoice = new Invoices({
            owner: user.companyName,
            ownerId: user.id,
            invoicePrice: params.invoicePrice,
            totalPrice: params.totalPrice,
            offerPrice: params.offerPrice,
            productDescription: params.productDescription,
            booked: params.booked
        })
        await invoice.save();
        res.send({ success: true, invoice });
    } catch (error) {
        throw new Error(error);
    }
}

async function list(req, res) {
    const invoices = await Invoices.find({});
    if (req.canTrade) {
        return res.send({ success: true, invoices, canTrade: req.canTrade });
    }

    const privateInvoice = invoices.map(invoice => ({
        ...invoice._doc,
        owner: 'Hidden',
        ownerId: 'Hidden',
        productDescription: 'Hidden',
        booked: invoice.booked.map(by => ({
            ...by._doc,
            name: 'Hidden'
        })),
    }));
    
    res.send({ success: true, invoices: privateInvoice, canTrade: req.canTrade });
}

module.exports = {
    newInvoice,
    list
};