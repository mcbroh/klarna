const Joi = require('@hapi/joi');

const buyers = Joi.object().keys({
    name: Joi.string().max(100).required(),
    amount: Joi.string().max(100).required(),
});

const createInvoiceValidation = Joi.object({
    invoicePrice: Joi.string().max(20).required(),
    totalPrice: Joi.string().max(20).required(),
    productDescription: Joi.string().max(100).required(),
    booked: Joi.array().items(buyers),
});

module.exports = {
    createInvoiceValidation,
};
