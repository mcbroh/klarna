const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Required = {
    type: String,
    required: true,
};

let InvoiceSchema = new Schema({
    owner: Required,
    ownerId: Required,
    invoicePrice: Required,
    totalPrice: Required,
    productDescription: Required,
    booked: [{
        name: {type: String},
        amount: {type: String},
    }],
    conditions: {
        type: String,
        default: `
        Sed vel cursus nisl. Duis vitae tincidunt odio, quis ullamcorper diam. Praesent at semper dui. Nulla facilisi. 
        Nunc vitae sapien ligula. Fusce diam lacus, dignissim vitae dolor sit amet, ultrices tempus magna. Maecenas 
        vitae ante in augue dictum cursus eget eu dui. Etiam et efficitur purus, vitae auctor tellus. Proin pellentesque 
        ex quis dolor posuere, rhoncus dictum lacus imperdiet. Aenean vitae diam efficitur, rhoncus ipsum eget, blandit 
        lorem. Suspendisse lacinia, ante gravida accumsan tristique, metus risus dapibus turpis, id pretium eros orci in 
        dolor. Aliquam semper pulvinar ipsum, non mattis nulla efficitur eu. Phasellus at mattis tortor. Pellentesque 
        convallis, eros eu malesuada egestas, augue ex lacinia dolor, sed viverra dui odio id orci. Sed quis mauris sed 
        justo vestibulum placerat in sollicitudin leo. Pellentesque luctus, nisl eget vestibulum auctor, leo ligula 
        suscipit orci, non consequat odio eros nec neque.`,
    }
});

module.exports = mongoose.model('LetsDealB2BInvoices', InvoiceSchema);