const express    = require("express");
const router = new express.Router();

const controller = require("../controllers");
const jwt = require('../../services/jwt');

router.post("/new", jwt.newUser, controller.createUser);
router.post("/login", jwt.userLogin, controller.loginUser);
router.post("/update", jwt.authenticateToken, controller.updateUser);
router.post("/delete", jwt.authenticateToken, controller.deleteUser);
router.get("/all", jwt.authenticateToken, controller.listUsers);
router.get("/me", jwt.authenticateToken, controller.getUser);


module.exports = router;