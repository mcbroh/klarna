require('dotenv').config();

const User = require('../models');
const schema = require('./validationSchema');

async function createUser(req, res) {
    const newUser = req.body;
    try {
        await schema.createUserValidation.validateAsync(newUser);
    } catch (error) {
        return res.send({ success: false, error: error.details[0].message })
    }

    try {
        const user = new User({
            contactEmail: newUser.email,
            contactName: newUser.contactName,
            contactTelephone: newUser.contactTelephone,
            companyName: newUser.companyName,
            companyEmail: newUser.companyEmail,
            companyTelephone: newUser.companyTelephone,
            country: newUser.country,
            password: newUser.password
        });
    
        await user.save();
        res.send({ success: true, user }); 
    } catch (error) {
        res.status(500).send({
            success: false,
            message: error.message
        });
    }
    
}

async function loginUser(req, res) {
    res.send({ success: true, user: req.user, token: req.token });
}

async function updateUser(req, res) {
    res.send({ success: true, message: 'works' });
}

async function deleteUser(req, res) {
    res.send({ success: true, message: 'works' });
}

async function listUsers(req, res) {
    res.send({ success: true, message: 'works' });
}

async function getUser(req, res) {
    res.send({ success: true, message: 'works' });
}

module.exports = {
    loginUser,
    createUser,
    updateUser,
    deleteUser,
    listUsers,
    getUser
};