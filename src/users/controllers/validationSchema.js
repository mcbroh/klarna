const Joi = require('@hapi/joi');

const createUserValidation = Joi.object({
    contactName: Joi.string().max(100).required(),
    email: Joi.string().email().required(),
    contactTelephone: Joi.string().max(15).required(),
    companyName: Joi.string().max(100).required(),
    companyEmail: Joi.string().email().required(),
    companyTelephone: Joi.string().max(15).required(),
    password: Joi.string().max(999).required(),
    country: Joi.string().max(20).required(),
});

const loginUserValidation = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().max(30).required(),
});

module.exports = {
    createUserValidation,
    loginUserValidation,
};