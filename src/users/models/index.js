const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

const Required = {
    type: String,
    required: true,
};

let UserSchema = new Schema({
    contactName: {
        ...Required,
        max: 100
    },
    contactEmail: {
        ...Required,
        unique: true
    },
    contactTelephone: {
        type: String,
    },
    companyName: {
        ...Required,
        unique: true,
        max: 100
    },
    companyEmail: {
        ...Required,
        unique: true
    },
    companyTelephone: {
        type: String,
    },
    password: {
        type: String,
    },
    country: {
        type: String,
        max: 5,
        default: 'Nigeria',
    },
    logo: {
        key: {type: String},
        url: {type: String},
        imgType: {type: String},
    },
}, { toJSON: { 
    virtuals: true,
    transform: function (doc, user) {
      delete user._id;
      delete user.password;
      return user;
    }

  }, timestamps: true });


UserSchema.plugin(uniqueValidator);

module.exports = mongoose.model('LetsDealB2BUser', UserSchema);