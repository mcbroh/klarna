require("dotenv").config();
const jwt = require("jsonwebtoken");
const bcrypt = require('bcrypt');

const User = require('../users/models');
const schema = require('../users/controllers/validationSchema');

const jwtToken = process.env.JWT_ACCESS_TOKEN_SECRET;

async function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]

    if (token == null) return res.sendStatus(401);
    
    try {
        jwt.verify(token, jwtToken, async function (err, decoded) {
            if (decoded) {
                req.user = await User.findById(decoded.userId);
                next();
            } else {
                return res.status(401);
            }
            
        });
    } catch (error) {
        return res.sendStatus(401);
    }

}

function newUser(req, res, next) {

    if (!req.body.password) {
        return res.send({
            success: false, message: 'Password required'
        })
    }

    const hash = 10;

    try {
        bcrypt.hash(req.body.password, hash, (err, encrypted) => {
            req.body.password = encrypted
            next()
        })
    } catch (error) {
        return res.status(500).send({
            success: false, message: error.message
        })
    }

}

async function userLogin(req, res, next) {

    try {
        await schema.loginUserValidation.validateAsync(req.body);
    } catch (error) {
        return res.send({ success: false, error: error.details[0].message })
    }

    try {
        const user = await User.findOne({ contactEmail: req.body.email });
        console.log(user);
        
        if (!user) {
            return res.send({
                success: false, 
                message: 'Email or password incorrect!'
            });
        }
        bcrypt.compare(req.body.password, user.password, function (err, result) {
            if (result) {
                req.user = user;
                req.token = generateAccessToken(user.id);
                next()
            } else {                
                return res.send({
                    success: false, 
                    message: 'Email or password incorrect!'
                });
            }
        });
    } catch (error) {
        return res.status(500).send({
            success: false, message: error.message
        })
    }
}

async function canTrade(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    
    try {
        jwt.verify(token, jwtToken, async function (err, decoded) {
            if (decoded) {
                const user = await User.findById(decoded.userId);
                req.canTrade = Boolean(user);
                next();
            } else {
                req.canTrade = false;
                return next();
            }
            
        });
    } catch (error) {
        req.canTrade = false;
        return next();
    }

}

function generateAccessToken(userId) {
    return jwt.sign({ userId: userId }, jwtToken, { expiresIn: '7d' });
}

module.exports = {
    newUser,
    userLogin,
    canTrade,
    authenticateToken,
};