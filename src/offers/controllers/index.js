const Offers = require('../models');
const validator = require('./validationSchema');

async function newOffer(req, res) {
    const params = req.body;
    try {
        await validator.createOfferValidation.validateAsync(params);
    } catch (error) {
        return res.send({ success: false, error: error.details[0].message });
    }

    try {
        const userId = req.user.id;
        const offer = new Offers({
            owner: userId,
            images: params.images || [],
            marketPrice: params.marketPrice,
            offerPrice: params.offerPrice,
            productDescription: params.productDescription,
            qualifyingPieces: params.qualifyingPieces,
            details: params.details
        })
        await offer.save();
        res.send({ success: true, offer });
    } catch (error) {
        throw new Error(error);
    }
}

async function list(req, res) {
    const offers = await Offers.find({});
    res.send({ success: true, offers });
}

module.exports = {
    newOffer,
    list
};