const Joi = require('@hapi/joi');

const createOfferValidation = Joi.object({
    marketPrice: Joi.string().max(20).required(),
    offerPrice: Joi.string().max(20).required(),
    qualifyingPieces: Joi.string().max(100).required(),
    details: Joi.string().max(999).required(),
    productDescription: Joi.string().max(300).required(),
    images: Joi.optional(),
});


module.exports = {
    createOfferValidation,
};
