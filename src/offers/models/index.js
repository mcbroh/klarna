const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

const Required = {
    type: String,
    required: true,
};

let OfferSchema = new Schema({
    owner: Required,
    marketPrice: Required,
    offerPrice: Required,
    productDescription: Required,
    qualifyingPieces: Required,
    details: Required,
    images: [{
        type: String
    }],
    booked: [{
        name: {type: String},
        amount: {type: String},
    }],
    conditions: {
        type: String,
        default: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sagittis enim in sem fermentum, 
        tincidunt sodales augue scelerisque. Suspendisse potenti. Phasellus viverra, urna at eleifend vulputate, 
        nisl turpis malesuada nisi, vel mattis nunc tortor ut dui. Aliquam lorem sem, lobortis sed sem a, vehicula 
        congue nisi. Donec nisl sapien, ultricies eu molestie vitae, sodales ac ex. Donec id diam efficitur, cursus ex 
        at, pharetra nulla. Nam cursus arcu eget mi tincidunt, in consectetur felis mollis.
        In et lectus enim. Aliquam ex ante, vehicula pellentesque vestibulum vitae, iaculis non sem. Nullam felis velit, 
        auctor vel fringilla a, rhoncus id neque. Quisque feugiat, velit ac mollis ullamcorper, leo metus tristique velit, 
        ut luctus nibh elit tristique tellus. Maecenas laoreet felis vel sem ultricies, sed feugiat enim accumsan. 
        Pellentesque posuere ipsum nec pretium scelerisque. Vivamus pellentesque sapien lacus, lacinia suscipit 
        erat scelerisque ac.
        Sed vel cursus nisl. Duis vitae tincidunt odio, quis ullamcorper diam. Praesent at semper dui. Nulla facilisi. 
        Nunc vitae sapien ligula. Fusce diam lacus, dignissim vitae dolor sit amet, ultrices tempus magna. Maecenas 
        vitae ante in augue dictum cursus eget eu dui. Etiam et efficitur purus, vitae auctor tellus. Proin pellentesque 
        ex quis dolor posuere, rhoncus dictum lacus imperdiet. Aenean vitae diam efficitur, rhoncus ipsum eget, blandit 
        lorem. Suspendisse lacinia, ante gravida accumsan tristique, metus risus dapibus turpis, id pretium eros orci in 
        dolor. Aliquam semper pulvinar ipsum, non mattis nulla efficitur eu. Phasellus at mattis tortor. Pellentesque 
        convallis, eros eu malesuada egestas, augue ex lacinia dolor, sed viverra dui odio id orci. Sed quis mauris sed 
        justo vestibulum placerat in sollicitudin leo. Pellentesque luctus, nisl eget vestibulum auctor, leo ligula 
        suscipit orci, non consequat odio eros nec neque.`,
    }
});


OfferSchema.plugin(uniqueValidator);

module.exports = mongoose.model('LetsDealB2BOffers', OfferSchema);