require('dotenv').config({ path: '.env.local' });
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const fileupload = require("express-fileupload");

const app = express();

mongoose.Promise = global.Promise;
mongoose.disconnect();
mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
}).then(() => {
  console.log("Connected to Database");
}).catch((err) => {
  console.log("Not Connected to Database ERROR! ", err);
});

app.use(bodyParser.urlencoded({ extended: false }))
    .use(bodyParser.json())
    .use(fileupload())
    .use("/users", require('./src/users/routes'))
    .use("/offers", require('./src/offers/routes'))
    .use("/invoice", require('./src/Invoices/routes'));



if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'));

  const path = require('path');
  app.get('*', (req,res) => {
      res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
  })

}

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`app running on port ${PORT}`);
});
